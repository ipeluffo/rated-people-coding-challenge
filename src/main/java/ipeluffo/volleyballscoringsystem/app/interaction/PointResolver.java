package ipeluffo.volleyballscoringsystem.app.interaction;

import ipeluffo.volleyballscoringsystem.model.VolleyballTeam;

/**
 * Created by ipeluffo on 7/21/15.
 */
public interface PointResolver {

    VolleyballTeam getTeamThatWonPoint();

}
