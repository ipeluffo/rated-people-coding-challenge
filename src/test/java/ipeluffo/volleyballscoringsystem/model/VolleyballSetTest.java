package ipeluffo.volleyballscoringsystem.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

/**
 * Created by ipeluffo on 7/20/15.
 */
public class VolleyballSetTest {

    private VolleyballTeam teamA;
    private VolleyballTeam teamB;
    private int pointsSet;
    private int maxPoints;

    private VolleyballSet set;

    @Before
    public void beforeEachMethod() {
        teamA = mock(VolleyballTeam.class);
        teamB = mock(VolleyballTeam.class);
        pointsSet = 21;
        maxPoints = 50;

        set = new VolleyballSet(teamA, teamB, pointsSet, maxPoints);
    }

    @Test
    public void testConstructor() {
        assertNotNull(set);
        assertEquals(teamA, set.getTeamA());
        assertEquals(teamB, set.getTeamB());
        assertEquals(pointsSet, set.getPointsSet());
        assertEquals(maxPoints, set.getMaxPoints());
        assertEquals(0, set.teamScore(teamA));
        assertEquals(0, set.teamScore(teamB));
        assertFalse(set.isFinished());
        assertNull(set.getWinner());
    }

    @Test
    public void testScoreOnePoint() {
        set.scorePoint(teamA);

        assertEquals(1, set.teamScore(teamA));
        assertEquals(0, set.teamScore(teamB));
        assertFalse(set.isFinished());
        assertNull(set.getWinner());
    }

    @Test
    public void testTeamWin() {
        for (int i = 0; i < 21; i++) {
            set.scorePoint(teamA);
        }

        assertEquals(21, set.teamScore(teamA));
        assertEquals(0, set.teamScore(teamB));
        assertTrue(set.isFinished());
        assertEquals(teamA, set.getWinner());
    }

    @Test
    public void testNoWinnerIfThereIsNoDifferenceOfTwo() {
        for (int i = 0; i < 20; i++) {
            set.scorePoint(teamA);
            set.scorePoint(teamB);
        }

        set.scorePoint(teamA);

        assertEquals(21, set.teamScore(teamA));
        assertEquals(20, set.teamScore(teamB));
        assertFalse(set.isFinished());
        assertNull(set.getWinner());
    }

    @Test
    public void testTeamAWinsWithDifferenceOfTwo() {
        for (int i = 0; i < 20; i++) {
            set.scorePoint(teamA);
            set.scorePoint(teamB);
        }

        set.scorePoint(teamA);
        set.scorePoint(teamA);

        assertEquals(22, set.teamScore(teamA));
        assertEquals(20, set.teamScore(teamB));
        assertTrue(set.isFinished());
        assertEquals(teamA, set.getWinner());
    }

    @Test
    public void testTeamBWinsWithDifferenceOfTwo() {
        for (int i = 0; i < 20; i++) {
            set.scorePoint(teamA);
            set.scorePoint(teamB);
        }

        set.scorePoint(teamB);
        set.scorePoint(teamB);

        assertEquals(20, set.teamScore(teamA));
        assertEquals(22, set.teamScore(teamB));
        assertTrue(set.isFinished());
        assertEquals(teamB, set.getWinner());
    }

    @Test
    public void testTeamAWinsReachingMaxPointsWithNoDifferenceOfTwo() {
        for (int i = 0; i < 49; i++) {
            set.scorePoint(teamA);
            set.scorePoint(teamB);
        }

        set.scorePoint(teamA);

        assertEquals(50, set.teamScore(teamA));
        assertEquals(49, set.teamScore(teamB));
        assertTrue(set.isFinished());
        assertEquals(teamA, set.getWinner());
    }

    @Test
    public void testTeamBWinsReachingMaxPointsWithNoDifferenceOfTwo() {
        for (int i = 0; i < 49; i++) {
            set.scorePoint(teamA);
            set.scorePoint(teamB);
        }

        set.scorePoint(teamB);

        assertEquals(49, set.teamScore(teamA));
        assertEquals(50, set.teamScore(teamB));
        assertTrue(set.isFinished());
        assertEquals(teamB, set.getWinner());
    }

    @Test
    public void testTeamAWinsByTwoWithoutCheckingMaxPoints() {
        set = new VolleyballSet(teamA, teamB, 21);

        for (int i = 0; i < 100; i++) {
            set.scorePoint(teamA);
            set.scorePoint(teamB);
        }

        set.scorePoint(teamA);
        set.scorePoint(teamA);

        assertEquals(102, set.teamScore(teamA));
        assertEquals(100, set.teamScore(teamB));
        assertTrue(set.isFinished());
        assertEquals(teamA, set.getWinner());
    }

    @Test
    public void testNoWinnerWithoutCheckingMaxPoints() {
        set = new VolleyballSet(teamA, teamB, 21);

        for (int i = 0; i < 100; i++) {
            set.scorePoint(teamA);
            set.scorePoint(teamB);
        }

        set.scorePoint(teamA);

        assertEquals(101, set.teamScore(teamA));
        assertEquals(100, set.teamScore(teamB));
        assertFalse(set.isFinished());
        assertNull(set.getWinner());
    }

}
