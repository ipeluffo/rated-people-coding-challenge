package ipeluffo.volleyballscoringsystem.app.console.reader;

/**
 * Created by ipeluffo on 7/21/15.
 */
public class ConsoleUserInputReader {

    ConsoleReader consoleReader;

    public ConsoleUserInputReader(ConsoleReader consoleReader) {
        this.consoleReader = consoleReader;
    }

    public boolean readBoolean(String message, boolean defaultValue) {
        while (true) {
            printMessage(message, defaultValue ? "yes" : "no");
            String userInput = consoleReader.nextLine();

            switch (userInput) {
                case "yes"  :   return true;
                case "no"   :   return false;
                case ""     :   return defaultValue;
            }
        }
    }

    public int readInt(String message, int defaultValue, int lowerLimit, int upperLimit) {
        boolean correctValue = false;
        int inputInt = 0;

        while (!correctValue) {
            try {
                printMessage(message, defaultValue);
                String input = consoleReader.nextLine();
                inputInt = input.isEmpty() ? defaultValue : Integer.parseInt(input);

                if (inputInt < lowerLimit || inputInt > upperLimit) {
                    System.out.println("The value is out of range ["+lowerLimit+", "+upperLimit+"]");
                } else {
                    correctValue = true;
                }

            } catch (NumberFormatException e) {
                System.out.println("Please, enter a correct integer value");
            }
        }

        return inputInt;
    }

    public String readString(String message, String defaultValue) {
        printMessage(message, defaultValue);
        String userInput = consoleReader.nextLine();

        if (userInput.isEmpty()) {
            return defaultValue;
        }

        return userInput;
    }

    private void printMessage(String message, Object defaultValue) {
        System.out.print(message+" (default: "+defaultValue+"): ");
    }

}
