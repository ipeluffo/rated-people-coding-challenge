package ipeluffo.volleyballscoringsystem.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ipeluffo on 7/20/15.
 */
public class VolleyballSet {

    private Map<VolleyballTeam, Integer> teamsScores = new HashMap<>();
    private VolleyballTeam teamA;
    private VolleyballTeam teamB;
    private int pointsSet = 21;
    private int maxPoints = -1;
    private boolean finished = false;
    private VolleyballTeam winner = null;

    public VolleyballSet(VolleyballTeam teamA, VolleyballTeam teamB, int pointsSet, int maxPoints) {
        teamsScores.put(teamA, 0);
        teamsScores.put(teamB, 0);
        this.teamA = teamA;
        this.teamB = teamB;
        this.pointsSet = pointsSet;
        this.maxPoints = maxPoints;
    }

    public VolleyballSet(VolleyballTeam teamA, VolleyballTeam teamB, int pointsSet) {
        teamsScores.put(teamA, 0);
        teamsScores.put(teamB, 0);
        this.teamA = teamA;
        this.teamB = teamB;
        this.pointsSet = pointsSet;
    }

    public void scorePoint(VolleyballTeam team) {
        if (this.finished)
            return;

        if (teamA.equals(team))
            processPoint(teamA, teamB);
        else if (teamB.equals(team))
            processPoint(teamB, teamA);
        else
            throw new IllegalArgumentException("The team is not a valid team for the set");
    }

    private void processPoint(VolleyballTeam teamWonPoint, VolleyballTeam teamLostPoint) {
        teamsScores.put(teamWonPoint, teamsScores.get(teamWonPoint)+1);

        int scoreTeamWonPoint = teamsScores.get(teamWonPoint);
        int scoreTeamLostPoint = teamsScores.get(teamLostPoint);

        if (maxPoints > 0)
            finished = (scoreTeamWonPoint == maxPoints) || (scoreTeamWonPoint >= pointsSet && (scoreTeamWonPoint-scoreTeamLostPoint) > 1);
        else
            finished = scoreTeamWonPoint >= pointsSet && (scoreTeamWonPoint-scoreTeamLostPoint) > 1;

        if (finished)
            winner = teamWonPoint;
    }

    public VolleyballTeam getTeamA() {
        return teamA;
    }

    public VolleyballTeam getTeamB() {
        return teamB;
    }

    public int getPointsSet() {
        return pointsSet;
    }

    public int getMaxPoints() {
        return maxPoints;
    }

    public boolean isFinished() {
        return finished;
    }

    public VolleyballTeam getWinner() {
        return winner;
    }

    public int teamScore(VolleyballTeam team) {
        return this.teamsScores.get(team);
    }

}
