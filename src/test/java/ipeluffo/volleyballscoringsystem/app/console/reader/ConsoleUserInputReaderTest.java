package ipeluffo.volleyballscoringsystem.app.console.reader;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

/**
 * Created by ipeluffo on 7/21/15.
 */
public class ConsoleUserInputReaderTest {

    private ConsoleReader consoleReaderMock;

    @Before
    public void beforeEachMethod() {
        consoleReaderMock = Mockito.mock(ConsoleReader.class);
    }

    @Test
    public void testReadBoolean() {
        when(consoleReaderMock.nextLine()).thenReturn("yes");

        ConsoleUserInputReader inputReader = new ConsoleUserInputReader(consoleReaderMock);
        boolean result = inputReader.readBoolean("message", false);

        assertTrue(result);
        verify(consoleReaderMock, times(1)).nextLine();
    }

    @Test
    public void testReadBooleanDefaultValue() {
        when(consoleReaderMock.nextLine()).thenReturn("");

        ConsoleUserInputReader inputReader = new ConsoleUserInputReader(consoleReaderMock);
        boolean result = inputReader.readBoolean("message", true);

        assertTrue(result);
        verify(consoleReaderMock, times(1)).nextLine();
    }

    @Test
    public void testReadBooleanWrongInput() {
        when(consoleReaderMock.nextLine()).thenReturn("wrong", "yes");

        ConsoleUserInputReader inputReader = new ConsoleUserInputReader(consoleReaderMock);
        boolean result = inputReader.readBoolean("message", false);

        assertTrue(result);
        verify(consoleReaderMock, times(2)).nextLine();
    }

    @Test
    public void testReadInt() {
        when(consoleReaderMock.nextLine()).thenReturn("10");
        int expectedResult = 10;

        ConsoleUserInputReader inputReader = new ConsoleUserInputReader(consoleReaderMock);
        int result = inputReader.readInt("message", 5, 0, 20);

        assertEquals(expectedResult, result);
        verify(consoleReaderMock, times(1)).nextLine();
    }

    @Test
    public void testReadIntDefaultValue() {
        when(consoleReaderMock.nextLine()).thenReturn("");
        int expectedResult = 5;

        ConsoleUserInputReader inputReader = new ConsoleUserInputReader(consoleReaderMock);
        int result = inputReader.readInt("message", 5, 0, 20);

        assertEquals(expectedResult, result);
        verify(consoleReaderMock, times(1)).nextLine();
    }

    @Test
    public void testReadIntWrongInput() {
        when(consoleReaderMock.nextLine()).thenReturn("wrong", "25", "-1", "12");
        int expectedResult = 12;

        ConsoleUserInputReader inputReader = new ConsoleUserInputReader(consoleReaderMock);
        int result = inputReader.readInt("message", 5, 0, 20);

        assertEquals(expectedResult, result);
        verify(consoleReaderMock, times(4)).nextLine();
    }

    @Test
    public void testReadString() {
        String expectedResult = "string";
        when(consoleReaderMock.nextLine()).thenReturn(expectedResult);

        ConsoleUserInputReader inputReader = new ConsoleUserInputReader(consoleReaderMock);
        String result = inputReader.readString("message", "default");

        assertEquals(expectedResult, result);
        verify(consoleReaderMock, times(1)).nextLine();
    }

    @Test
    public void testReadStringDefaultValue() {
        String expectedResult = "default";
        when(consoleReaderMock.nextLine()).thenReturn("");

        ConsoleUserInputReader inputReader = new ConsoleUserInputReader(consoleReaderMock);
        String result = inputReader.readString("message", expectedResult);

        assertEquals(expectedResult, result);
        verify(consoleReaderMock, times(1)).nextLine();
    }

}
