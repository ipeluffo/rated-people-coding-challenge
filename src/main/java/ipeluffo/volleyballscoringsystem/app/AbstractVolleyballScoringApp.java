package ipeluffo.volleyballscoringsystem.app;

import ipeluffo.volleyballscoringsystem.app.interaction.PointResolver;
import ipeluffo.volleyballscoringsystem.model.VolleyballMatch;
import ipeluffo.volleyballscoringsystem.model.VolleyballTeam;

/**
 * Created by ipeluffo on 7/21/15.
 */
public abstract class AbstractVolleyballScoringApp {

    protected static final int DEFAULT_NUMBER_SETS = 3;
    protected static final int DEFAULT_NUMBER_POINTS_SET = 21;
    protected static final int DEFAULT_NUMBER_POINTS_DECIDER_SET = 15;
    protected static final int DEFAULT_MAX_NUMBER = 50;
    protected static final int DEFAULT_POINT_RESOLVER = 2;

    public final void runMatch() {
        VolleyballMatch match = createMatch();
        PointResolver pointResolver = createPointResolver();

        while (!match.isFinished()) {
            VolleyballTeam teamWonPoint = pointResolver.getTeamThatWonPoint();
            match.scorePoint(teamWonPoint);
            printPartialResult();
        }

        if (match.isTie()) {
            printTie();
        } else {
            printWinnerScore(match);
        }
    }

    protected abstract VolleyballMatch createMatch();

    protected abstract PointResolver createPointResolver();

    protected abstract void printPartialResult();

    protected abstract void printTie();

    protected abstract void printWinnerScore(VolleyballMatch match);

}
