package ipeluffo.volleyballscoringsystem;

import ipeluffo.volleyballscoringsystem.app.AbstractVolleyballScoringApp;
import ipeluffo.volleyballscoringsystem.app.console.ConsoleVolleyballScoringApp;

/**
 * Created by ipeluffo on 7/20/15.
 */
public class Main {

    public static void main(String[] args) {
        AbstractVolleyballScoringApp app = new ConsoleVolleyballScoringApp();
        app.runMatch();
    }

}
