package ipeluffo.volleyballscoringsystem.app.console.interaction;

import ipeluffo.volleyballscoringsystem.app.console.reader.ConsoleReader;
import ipeluffo.volleyballscoringsystem.model.VolleyballTeam;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

/**
 * Created by ipeluffo on 7/21/15.
 */
public class HumanPointResolverTest {

    private ConsoleReader consoleReaderMock;
    private VolleyballTeam teamAMock;
    private VolleyballTeam teamBMock;

    @Before
    public void beforeEachMethod() {
        consoleReaderMock = mock(ConsoleReader.class);
        teamAMock = mock(VolleyballTeam.class);
        teamBMock = mock(VolleyballTeam.class);
    }

    @Test
    public void testGetTeamThatWonPointWithTeamA() {
        when(consoleReaderMock.nextLine()).thenReturn("1");

        HumanPointResolver humanPointResolver = new HumanPointResolver(teamAMock, teamBMock, consoleReaderMock);
        VolleyballTeam team = humanPointResolver.getTeamThatWonPoint();

        assertNotNull(team);
        assertEquals(teamAMock, team);
        verify(consoleReaderMock, times(1)).nextLine();
    }

    @Test
    public void testGetTeamThatWonPointWithTeamB() {
        when(consoleReaderMock.nextLine()).thenReturn("2");

        HumanPointResolver humanPointResolver = new HumanPointResolver(teamAMock, teamBMock, consoleReaderMock);
        VolleyballTeam team = humanPointResolver.getTeamThatWonPoint();

        assertNotNull(team);
        assertEquals(teamBMock, team);
        verify(consoleReaderMock, times(1)).nextLine();
    }

    @Test
    public void testGetTeamThatWonPointWithWrongInputs() {
        when(consoleReaderMock.nextLine()).thenReturn("", "-1", "3", "2");

        HumanPointResolver humanPointResolver = new HumanPointResolver(teamAMock, teamBMock, consoleReaderMock);
        VolleyballTeam team = humanPointResolver.getTeamThatWonPoint();

        assertNotNull(team);
        assertEquals(teamBMock, team);
        verify(consoleReaderMock, times(4)).nextLine();
    }

}
