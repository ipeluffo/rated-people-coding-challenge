package ipeluffo.volleyballscoringsystem.app.console.interaction;

import ipeluffo.volleyballscoringsystem.app.console.reader.ConsoleReader;
import ipeluffo.volleyballscoringsystem.app.interaction.PointResolver;
import ipeluffo.volleyballscoringsystem.model.VolleyballTeam;

import java.util.InputMismatchException;

/**
 * Created by ipeluffo on 7/21/15.
 */
public class HumanPointResolver implements PointResolver {

    private VolleyballTeam teamA;
    private VolleyballTeam teamB;

    private ConsoleReader consoleReader;

    public HumanPointResolver(VolleyballTeam teamA, VolleyballTeam teamB, ConsoleReader consoleReader) {
        this.teamA = teamA;
        this.teamB = teamB;
        this.consoleReader = consoleReader;
    }

    @Override
    public VolleyballTeam getTeamThatWonPoint() {
        boolean correctInput = false;
        String teamInput = "1";

        System.out.println("Choose which team won the point");
        System.out.println("1: "+teamA);
        System.out.println("2: "+teamB);

        while (!correctInput) {
            try {
                System.out.print("Point winner: ");
                teamInput = consoleReader.nextLine();
                if (!teamInput.equals("1") && !teamInput.equals("2")) {
                    System.out.println("Please choose a correct option value: 1 or 2");
                } else {
                    correctInput = true;
                }
            } catch (InputMismatchException e) {
                System.out.println("Please choose a correct option value: 1 or 2");
            }
        }

        if (teamInput.equals("1")) {
            return teamA;
        }

        return teamB;
    }
}
