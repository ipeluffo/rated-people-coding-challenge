package ipeluffo.volleyballscoringsystem.app.console;

import ipeluffo.volleyballscoringsystem.app.AbstractVolleyballScoringApp;
import ipeluffo.volleyballscoringsystem.app.console.interaction.HumanPointResolver;
import ipeluffo.volleyballscoringsystem.app.console.reader.ConsoleReader;
import ipeluffo.volleyballscoringsystem.app.console.reader.ConsoleUserInputReader;
import ipeluffo.volleyballscoringsystem.app.interaction.PointResolver;
import ipeluffo.volleyballscoringsystem.app.interaction.RandomPointResolver;
import ipeluffo.volleyballscoringsystem.model.VolleyballMatch;
import ipeluffo.volleyballscoringsystem.model.VolleyballSet;
import ipeluffo.volleyballscoringsystem.model.VolleyballTeam;

import java.util.List;

/**
 * Created by ipeluffo on 7/21/15.
 */
public class ConsoleVolleyballScoringApp extends AbstractVolleyballScoringApp {

    private VolleyballTeam teamA;
    private VolleyballTeam teamB;
    private VolleyballMatch match;
    private PointResolver pointResolver;
    private ConsoleReader consoleReader;
    private ConsoleUserInputReader inputReader;

    public ConsoleVolleyballScoringApp() {
        consoleReader = new ConsoleReader();
        inputReader = new ConsoleUserInputReader(consoleReader);
    }

    @Override
    protected VolleyballMatch createMatch() {
        String nameTeamA = inputReader.readString("Name of team A", "Team A");
        String nameTeamB = inputReader.readString("Name of team B", "Team B");
        int setsCount = inputReader.readInt("Numbers of games/sets", DEFAULT_NUMBER_SETS, 1, Integer.MAX_VALUE);
        int pointsSet = inputReader.readInt("Number of points to win a game/set", DEFAULT_NUMBER_POINTS_SET, 1, Integer.MAX_VALUE);
        int pointsDeciderSet = inputReader.readInt("Number of points for the decider game/set", DEFAULT_NUMBER_POINTS_DECIDER_SET, 1, Integer.MAX_VALUE);

        boolean setMaxPoints = inputReader.readBoolean("Do you want to set a max number of points for sets? (yes or no)", false);
        int maxPoints = -1;
        if (setMaxPoints)
            maxPoints = inputReader.readInt("Max number of points", DEFAULT_MAX_NUMBER, pointsSet, Integer.MAX_VALUE);

        teamA = new VolleyballTeam(nameTeamA);
        teamB = new VolleyballTeam(nameTeamB);

        if (maxPoints > 0) {
            match = new VolleyballMatch(teamA, teamB, setsCount, pointsSet, pointsDeciderSet, maxPoints);
        } else {
            match = new VolleyballMatch(teamA, teamB, setsCount, pointsSet, pointsDeciderSet);
        }

        return match;
    }

    @Override
    protected PointResolver createPointResolver() {
        int versionPointResolver = inputReader.readInt(
                "Choose version\n"+
                        "Version 1) For every point, choose the team that won the point\n"+
                        "Version 2) For every point, the decision of who won the point is random\n", DEFAULT_POINT_RESOLVER, 1, 2);

        pointResolver = versionPointResolver == 1 ? new HumanPointResolver(teamA, teamB, consoleReader) : new RandomPointResolver(teamA, teamB);

        return pointResolver;
    }

    @Override
    protected void printPartialResult() {
        System.out.print(match);
    }

    @Override
    protected void printTie() {
        System.out.println("The match ended in a draw!");
    }

    @Override
    protected void printWinnerScore(VolleyballMatch match) {
        VolleyballTeam winner = match.getWinner();
        VolleyballTeam looser = winner.equals(teamA) ? teamB : teamA;

        List<VolleyballSet> sets = match.getSets();

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Team ").append(winner).append(" won ").append(match.teamSets(winner)).append("-").append(match.teamSets(looser)).append(" (");

        for (VolleyballSet set : sets) {
            if (set.isFinished()) {
                stringBuilder.append(set.teamScore(winner)).append("-").append(set.teamScore(looser)).append(", ");
            } else {
                break;
            }
        }

        stringBuilder.replace(stringBuilder.length() - 2, stringBuilder.length() - 1, ")");

        System.out.println(stringBuilder.toString());
    }
}
