package ipeluffo.volleyballscoringsystem.app.console.reader;

import java.util.Scanner;

/**
 * Created by ipeluffo on 7/21/15.
 *
 * This class was created because Mockito can't mock final classes like java.util.Scanner
 *
 */
public class ConsoleReader {

    private Scanner scanner;

    public ConsoleReader() {
        this.scanner = new Scanner(System.in);
    }

    public String nextLine() {
        return scanner.nextLine();
    }

    public short nextShort() {
        return scanner.nextShort();
    }

}
