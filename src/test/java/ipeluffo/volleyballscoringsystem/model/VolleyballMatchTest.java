package ipeluffo.volleyballscoringsystem.model;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

/**
 * Created by ipeluffo on 7/20/15.
 */
public class VolleyballMatchTest {

    private VolleyballTeam teamA;
    private VolleyballTeam teamB;
    private int setsCount;
    private int pointsSet;
    private int pointsDeciderSet;
    private int maxPoints;

    @Before
    public void beforeEachTest () {
        teamA = mock(VolleyballTeam.class);
        teamB = mock(VolleyballTeam.class);
        setsCount = 3;
        pointsSet = 21;
        pointsDeciderSet = 15;
        maxPoints = 50;
    }

    @Test
    public void testConstructorWithMaxPoints() {
        VolleyballMatch match = new VolleyballMatch(teamA, teamB, setsCount, pointsSet, pointsDeciderSet, maxPoints);

        List<VolleyballSet> sets = match.getSets();

        assertNotNull(match);
        assertEquals(setsCount, sets.size());
        assertNull(match.getWinner());
        assertFalse(match.isFinished());
        assertFalse(match.isTie());
        assertEquals(0, match.teamSets(teamA));
        assertEquals(0, match.teamSets(teamB));
        assertEquals(2, match.getSetsToWin());

        for (int i = 0; i < setsCount-1; i++) {
            assertEquals(pointsSet, sets.get(i).getPointsSet());
            assertEquals(maxPoints, sets.get(i).getMaxPoints());
        }

        // Verify last set (i.e. decider-set) has the correct amount of points
        assertEquals(pointsDeciderSet, sets.get(setsCount-1).getPointsSet());
        assertEquals(maxPoints, sets.get(setsCount-1).getMaxPoints());
    }

    @Test
    public void testConstructorWithoutMaxPoints() {
        VolleyballMatch match = new VolleyballMatch(teamA, teamB, setsCount, pointsSet, pointsDeciderSet);

        List<VolleyballSet> sets = match.getSets();

        assertNotNull(match);
        assertEquals(setsCount, sets.size());
        assertNull(match.getWinner());
        assertFalse(match.isFinished());
        assertFalse(match.isTie());
        assertEquals(0, match.teamSets(teamA));
        assertEquals(0, match.teamSets(teamB));
        assertEquals(2, match.getSetsToWin());

        for (int i = 0; i < setsCount-1; i++) {
            assertEquals(pointsSet, sets.get(i).getPointsSet());
            assertEquals(-1, sets.get(i).getMaxPoints());
        }

        // Verify last set (i.e. decider-set) has the correct amount of points
        assertEquals(pointsDeciderSet, sets.get(setsCount-1).getPointsSet());
        assertEquals(-1, sets.get(setsCount-1).getMaxPoints());
    }

    @Test
    public void testTeamAScoreOnePoint() {
        VolleyballMatch match = new VolleyballMatch(teamA, teamB, setsCount, pointsSet, pointsDeciderSet);

        match.scorePoint(teamA);

        assertFalse(match.isFinished());
        assertFalse(match.isTie());
        assertEquals(0, match.teamSets(teamA));
        assertEquals(0, match.teamSets(teamB));
        assertEquals(teamA, match.getTeamServing());
        assertNull(match.getWinner());
    }

    @Test
    public void testTeamBScoreOnePoint() {
        VolleyballMatch match = new VolleyballMatch(teamA, teamB, setsCount, pointsSet, pointsDeciderSet);

        match.scorePoint(teamB);

        assertFalse(match.isFinished());
        assertFalse(match.isTie());
        assertEquals(0, match.teamSets(teamA));
        assertEquals(0, match.teamSets(teamB));
        assertEquals(teamB, match.getTeamServing());
        assertNull(match.getWinner());
    }

    @Test
    public void testTeamAWinsOneSet() {
        VolleyballMatch match = new VolleyballMatch(teamA, teamB, setsCount, pointsSet, pointsDeciderSet);

        for (int i = 0; i < pointsSet; i++) {
            match.scorePoint(teamA);
        }

        assertFalse(match.isFinished());
        assertFalse(match.isTie());
        assertEquals(1, match.teamSets(teamA));
        assertEquals(0, match.teamSets(teamB));
        assertEquals(teamA, match.getTeamServing());
        assertNull(match.getWinner());
    }

    @Test
    public void testTeamAWinsMatch() {
        VolleyballMatch match = new VolleyballMatch(teamA, teamB, setsCount, pointsSet, pointsDeciderSet);

        for (int i = 0; i < pointsSet*2; i++) {
            match.scorePoint(teamA);
        }

        assertTrue(match.isFinished());
        assertFalse(match.isTie());
        assertEquals(2, match.teamSets(teamA));
        assertEquals(0, match.teamSets(teamB));
        assertEquals(teamA, match.getTeamServing());
        assertEquals(teamA, match.getWinner());
    }

    @Test
    public void testTeamAOneSetAndTeamBOneSetAndNoWinners() {
        VolleyballMatch match = new VolleyballMatch(teamA, teamB, setsCount, pointsSet, pointsDeciderSet);

        // Team A wins one set
        for (int i = 0; i < pointsSet; i++) {
            match.scorePoint(teamA);
        }

        // Team B wins one set
        for (int i = 0; i < pointsSet; i++) {
            match.scorePoint(teamB);
        }

        assertFalse(match.isFinished());
        assertFalse(match.isTie());
        assertEquals(1, match.teamSets(teamA));
        assertEquals(1, match.teamSets(teamB));
        assertEquals(teamB, match.getTeamServing());
        assertNull(match.getWinner());
    }

    @Test
    public void testTieIfSetsCountIsEven() {
        int evenSetsCount = 2;
        VolleyballMatch match = new VolleyballMatch(teamA, teamB, evenSetsCount, pointsSet, pointsDeciderSet);

        // Team A wins one set
        for (int i = 0; i < pointsSet; i++) {
            match.scorePoint(teamA);
        }

        // Team B wins decider set
        for (int i = 0; i < pointsDeciderSet; i++) {
            match.scorePoint(teamB);
        }

        assertTrue(match.isFinished());
        assertTrue(match.isTie());
        assertEquals(1, match.teamSets(teamA));
        assertEquals(1, match.teamSets(teamB));
        assertEquals(teamB, match.getTeamServing());
        assertNull(match.getWinner());
        assertEquals(2, match.getSetsToWin());
    }

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void testNotValidTeamScoreOnePoint() {
        exception.expect(IllegalArgumentException.class);

        VolleyballMatch match = new VolleyballMatch(teamA, teamB, setsCount, pointsSet, pointsDeciderSet);
        match.scorePoint(mock(VolleyballTeam.class));
    }

    @Test
    public void testScorePointsAfterFinishedMatch() {
        VolleyballMatch match = new VolleyballMatch(teamA, teamB, setsCount, pointsSet, pointsDeciderSet, maxPoints);

        for (int i = 0; i < pointsSet*2; i++) {
            match.scorePoint(teamB);
        }

        assertTrue(match.isFinished());
        assertFalse(match.isTie());
        assertEquals(0, match.teamSets(teamA));
        assertEquals(2, match.teamSets(teamB));
        assertEquals(teamB, match.getTeamServing());
        assertEquals(teamB, match.getWinner());

        // Score 100 extra points and verify everything is equals
        for (int i = 0; i < 100; i++)
            match.scorePoint(teamA);

        assertTrue(match.isFinished());
        assertFalse(match.isTie());
        assertEquals(0, match.teamSets(teamA));
        assertEquals(2, match.teamSets(teamB));
        assertEquals(teamB, match.getTeamServing());
        assertEquals(teamB, match.getWinner());
    }

}
