package ipeluffo.volleyballscoringsystem.model;

/**
 * Created by ipeluffo on 7/20/15.
 */
public class VolleyballTeam {

    private String name;

    public VolleyballTeam(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
