package ipeluffo.volleyballscoringsystem.app.interaction;

import ipeluffo.volleyballscoringsystem.model.VolleyballTeam;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

/**
 * Created by ipeluffo on 7/21/15.
 */
public class RandomPointResolverTest {

    private VolleyballTeam teamA;
    private VolleyballTeam teamB;

    @Before
    public void beforeEachMethod() {
        teamA = mock(VolleyballTeam.class);
        teamB = mock(VolleyballTeam.class);
    }

    @Test
    public void testGetRandomTeam() {
        RandomPointResolver pointResolver = new RandomPointResolver(teamA, teamB);

        VolleyballTeam team = pointResolver.getTeamThatWonPoint();

        assertNotNull(team);
        assertTrue(teamA.equals(team) || teamB.equals(team));
    }

}
