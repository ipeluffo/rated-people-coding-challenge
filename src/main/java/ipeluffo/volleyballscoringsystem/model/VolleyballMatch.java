package ipeluffo.volleyballscoringsystem.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ipeluffo on 7/20/15.
 */
public class VolleyballMatch {

    private Map<VolleyballTeam, Integer> teamsSets = new HashMap<>();
    private VolleyballTeam teamA;
    private VolleyballTeam teamB;
    private List<VolleyballSet> sets = new ArrayList<>();
    private VolleyballSet currentSet;
    private VolleyballTeam teamServing;
    private int setsCount = 3;
    private int setsToWin = 2;
    private boolean finished = false;
    private VolleyballTeam winner = null;
    private boolean tie = false;

    public VolleyballMatch(VolleyballTeam teamA, VolleyballTeam teamB, int setsCount, int pointsSet,
                           int pointsDeciderSet, int maxPoints) {
        this.teamA = teamA;
        this.teamB = teamB;
        this.teamServing = teamA;
        this.teamsSets.put(teamA, 0);
        this.teamsSets.put(teamB, 0);

        this.setsCount = setsCount;
        this.setsToWin = setsCount / 2 + 1;

        for (int i = 0; i < setsCount - 1; i++) {
            sets.add(new VolleyballSet(teamA, teamB, pointsSet, maxPoints));
        }

        // Add the last decider set
        sets.add(new VolleyballSet(teamA, teamB, pointsDeciderSet, maxPoints));

        this.currentSet = sets.get(0);
    }

    public VolleyballMatch(VolleyballTeam teamA, VolleyballTeam teamB, int setsCount, int pointsSet, int pointsDeciderSet) {
        this.teamA = teamA;
        this.teamB = teamB;
        this.teamServing = teamA;
        this.teamsSets.put(teamA, 0);
        this.teamsSets.put(teamB, 0);

        this.setsCount = setsCount;
        this.setsToWin = setsCount / 2 + 1;

        for (int i = 0; i < setsCount - 1; i++) {
            sets.add(new VolleyballSet(teamA, teamB, pointsSet));
        }

        // Add the last decider set
        sets.add(new VolleyballSet(teamA, teamB, pointsDeciderSet));

        this.currentSet = sets.get(0);
    }

    public void scorePoint(VolleyballTeam team) {
        if (finished)
            return;

        if (!teamA.equals(team) && !teamB.equals(team))
            throw new IllegalArgumentException("The team is not a valid team for the match");

        currentSet.scorePoint(team);
        teamServing = team;

        if (currentSet.isFinished()) {
            if (teamA.equals(team))
                checkIfMatchHasFinished(teamA);
            else if (teamB.equals(team))
                checkIfMatchHasFinished(teamB);
        }
    }

    private void checkIfMatchHasFinished(VolleyballTeam teamWonSet) {
        teamsSets.put(teamWonSet, teamsSets.get(teamWonSet) + 1);
        int setsTeamWon = teamsSets.get(teamWonSet);

        boolean noMoreSets = sets.indexOf(currentSet) == sets.size()-1;

        if (setsTeamWon == setsToWin) {
            finished = true;
            winner = teamWonSet;
        } else if (noMoreSets) {
            finished = true;
            tie = true;
        } else {
            currentSet = sets.get(sets.indexOf(currentSet)+1);
        }
    }

    @Override
    public String toString() {
        /*
         * Teams    Sets    Current Set     Serving
         * TeamA    1       12              x
         * TeamB    0       6
         */
        boolean teamAServing = teamServing.equals(teamA);

        return new StringBuilder()
                .append("Teams\tSets\tCurrent\tServing")
                .append("\n")
                .append(teamA).append("\t").append(teamsSets.get(teamA)).append("\t").append(currentSet.teamScore(teamA)).append("\t").append(teamAServing ? "x" : "")
                .append("\n")
                .append(teamB).append("\t").append(teamsSets.get(teamB)).append("\t").append(currentSet.teamScore(teamB)).append("\t").append(teamAServing ? "" : "x")
                .append("\n").append("\n")
                .toString();
    }

    public boolean isFinished() {
        return finished;
    }

    public VolleyballTeam getWinner() {
        return winner;
    }

    public boolean isTie() {
        return tie;
    }

    public List<VolleyballSet> getSets() {
        return sets;
    }

    public int teamSets(VolleyballTeam team) {
        return this.teamsSets.get(team);
    }

    public VolleyballTeam getTeamServing() {
        return teamServing;
    }

    public int getSetsToWin() {
        return setsToWin;
    }
}
