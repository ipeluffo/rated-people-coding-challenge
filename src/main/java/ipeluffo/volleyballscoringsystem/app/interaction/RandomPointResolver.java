package ipeluffo.volleyballscoringsystem.app.interaction;

import ipeluffo.volleyballscoringsystem.model.VolleyballTeam;

import java.util.Random;

/**
 * Created by ipeluffo on 7/21/15.
 */
public class RandomPointResolver implements PointResolver {

    private VolleyballTeam teams[] = new VolleyballTeam[2];
    private Random random = new Random(System.currentTimeMillis());

    public RandomPointResolver(VolleyballTeam teamA, VolleyballTeam teamB) {
        teams[0] = teamA;
        teams[1] = teamB;
    }

    @Override
    public VolleyballTeam getTeamThatWonPoint() {
        return teams[random.nextInt(2)];
    }
}
