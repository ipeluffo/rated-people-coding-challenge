package ipeluffo.volleyballscoringsystem.model;


import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by ipeluffo on 7/20/15.
 */
public class VolleyballTeamTest {

    @Test
    public void testConstructor() {
        String name = "team name";
        VolleyballTeam team = new VolleyballTeam(name);

        assertNotNull(team);
        assertEquals(name, team.getName());
    }

}
